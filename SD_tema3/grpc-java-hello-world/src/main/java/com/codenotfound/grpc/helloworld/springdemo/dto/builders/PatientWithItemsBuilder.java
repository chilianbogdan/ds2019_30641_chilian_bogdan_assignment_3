package com.codenotfound.grpc.helloworld.springdemo.dto.builders;

import com.codenotfound.grpc.helloworld.springdemo.dto.MedDTO;
import com.codenotfound.grpc.helloworld.springdemo.dto.PatientWithItemsDTO;
import com.codenotfound.grpc.helloworld.springdemo.entities.Med;
import com.codenotfound.grpc.helloworld.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class PatientWithItemsBuilder {
    private PatientWithItemsBuilder(){}

    public static PatientWithItemsDTO generateDTOFromEntity(Patient patient, List<Med> meds){
        List<MedDTO> dtos =  meds.stream()
                .map(MedBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new PatientWithItemsDTO(
                patient.getId(),
                patient.getName(),
                dtos);
    }

}
