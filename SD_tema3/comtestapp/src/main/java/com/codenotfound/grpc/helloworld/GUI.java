package com.codenotfound.grpc.helloworld;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
//Imports are listed in full to show what's being used
//could just import javax.swing.* and java.awt.* etc..
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GUI  {




    public GUI() {





        JFrame guiFrame = new JFrame();
        // guiFrame.getContentPane().add(new GUI());
//make sure the program exits when the frame closes
        guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        guiFrame.setTitle("Example GUI");
        guiFrame.setSize(300, 250);
//This will center the JFrame in the middle of the screen
        guiFrame.setLocationRelativeTo(null);
        JTextField myOutput = new JTextField("someInitialValusf e", 30);
        myOutput.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
//When the fruit of veg button is pressed
//the setVisible value of the listPanel and
//comboPanel is switched from true to
//value or vice versa.
                //  listPanel.setVisible(!listPanel.isVisible());
                //  comboPanel.setVisible(!comboPanel.isVisible());
            }
        });
        //Options for the JComboBox
        //String[] fruitOptions = {"Apple", "Apricot", "Banana"
        //       ,"Cherry", "Date", "Kiwi", "Orange", "Pear", "Strawberry"};
        //Options for the JList
        // String[] vegOptions = {"Asparagus", "Beans", "Broccoli", "Cabbage"
        //      , "Carrot", "Celery", "Cucumber", "Leek", "Mushroom"
        //      , "Pepper", "Radish", "Shallot", "Spinach", "Swede"
        //      , "Turnip"};
        //The first JPanel contains a JLabel and JCombobox
        //final JPanel comboPanel = new JPanel();
        // JLabel comboLbl = new JLabel("Fruits:");
        //JComboBox fruits = new JComboBox(fruitOptions);
//comboPanel.add(comboLbl);
//comboPanel.add(fruits);
        //Create the second JPanel. Add a JLabel and JList and
//make use the JPanel is not visible.
        //   final JPanel listPanel = new JPanel();
//listPanel.setVisible(false);
        //   JLabel listLbl = new JLabel("Vegetables:");
        //   JList vegs = new JList(vegOptions);
//vegs.setLayoutOrientation(JList.HORIZONTAL_WRAP);
//listPanel.add(listLbl);
//listPanel.add(vegs);
        JButton but = new JButton("Med");
//The ActionListener class is used to handle the
//event that happens when the user clicks the button.
//As there is not a lot that needs to happen we can
//define an anonymous inner class to make the code simpler.
        but.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
//When the fruit of veg button is pressed
//the setVisible value of the listPanel and
//comboPanel is switched from true to
//value or vice versa.
                //  listPanel.setVisible(!listPanel.isVisible());
                //  comboPanel.setVisible(!comboPanel.isVisible());
            }
        });
//The JFrame uses the BorderLayout layout manager.
//Put the two JPanels and JButton in different areas.
//guiFrame.add(comboPanel, BorderLayout.NORTH);
//guiFrame.add(listPanel, BorderLayout.CENTER);
        String allmed = "";
        try {

            URL url = new URL("http://localhost:8080/med");

            // read text returned by server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String line;
            while ((line = in.readLine()) != null) {
                Pattern p = Pattern.compile("\\{(.*?)\\}");
                Matcher m = p.matcher(line);
                int i = 0;

                while (m.find()) {
                    // m.group(i); //is your string. do what you want
                    //System.out.println(m.group(i));
                    allmed=allmed + "\n"+ m.group(i);
                }
            }
            in.close();

        } catch (MalformedURLException e) {
            System.out.println("Malformed URL: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("I/O Error: " + e.getMessage());
        }




        JTextArea area = new JTextArea(allmed);
        area.setBounds(10, 30, 200, 200);
        guiFrame.add(but, BorderLayout.SOUTH);
        guiFrame.add(area, BorderLayout.NORTH);
//make sure the JFrame is visible
        guiFrame.setVisible(true);





    }
}
