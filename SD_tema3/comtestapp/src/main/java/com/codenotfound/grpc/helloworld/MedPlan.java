package com.codenotfound.grpc.helloworld;

public class MedPlan {


        private int id;
        private String name;
        private String side;
        private String dosage;


        public MedPlan(int id, String name, String side, String dosage) {
            this.id = id;
            this.name = name;
            this.side = side;
            this.dosage = dosage;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getSide() {
            return side;
        }

        public String getDosage() {
            return dosage;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setSide(String side) {
            this.side = side;
        }

        public void setDosage(String dosage) {
            this.dosage = dosage;
        }


        public MedPlan(String line) {
            String[] split = line.split("\\t+");
            id = 1;
            side = split[0];
            dosage = split[1];
            name = split[2];
        }

        @Override
        public String toString() {
            return "{\n" +
                    "\"id=\": " + id + ",\n" +
                    "\"name\": \"" + name + "\",\n" +
                    "\"side\": \"" + side + "\",\n" +
                    "\"dosage\": \"" + dosage + "\"\n" +
                    '}';
        }
    }



