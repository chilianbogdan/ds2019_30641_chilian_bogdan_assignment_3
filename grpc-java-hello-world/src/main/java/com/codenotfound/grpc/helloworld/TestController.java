package com.codenotfound.grpc.helloworld;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    private final HelloWorldClient helloWorldClient;

    @Autowired
    public TestController(HelloWorldClient helloWorldClient) {
        this.helloWorldClient = helloWorldClient;
//        try {
          // helloWorldClient.sayHello("Teodora", "Moldovan");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @GetMapping(value = "/grpc")
    public String salute() {
        String message = this.helloWorldClient.sayHello("Teodora", "Moldovan");
        return message;
    }
}
